﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

namespace Minecraft
{
    public class BlockTag { }

    public class SpawnOneBlock : MonoBehaviour
    {
        public int HowManay = 1;
        public static EntityArchetype m_blockArchetype;

        [Header("Mesh Info")] public Mesh m_blockMesh;
        [Header("Nature Block Type")] public Material m_blockMaterial;

        public EntityManager m_manager;
        public Entity m_entities;
        public GameObject Prefab_ref;

        // 场景加载前运行
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            EntityManager manager = World.Active.GetOrCreateManager<EntityManager>();

            // 所有entity都会有位置属性，所以提前加进去
            m_blockArchetype = manager.CreateArchetype(typeof(Position));
        }

        //[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        void Start()
        {
            #region 程序生成entity
            m_manager = World.Active.GetOrCreateManager<EntityManager>();

            for (int i = 0; i < HowManay; ++i)
            {
                Entity entity = m_manager.CreateEntity(m_blockArchetype);
                m_manager.SetComponentData(entity, new Position { Value = new int3(2, 0, 0) });
                //m_manager.AddComponentData(entity, new BlockTag{});

                m_manager.AddSharedComponentData(entity,
                    // 给它材质和Mesh绘制出结果
                    new MeshInstanceRenderer
                    {
                        mesh = m_blockMesh,
                        material = m_blockMaterial
                    });
            }
            #endregion

            #region 使用prefab生成entity，只保留prefab中的指定内容，无关内容剔除掉，保证还是按ecs处理
            if (Prefab_ref)
            {
                NativeArray<Entity> entityArray = new NativeArray<Entity>(1, Allocator.Temp);
                m_manager.Instantiate(Prefab_ref, entityArray);

                m_manager.SetComponentData(entityArray[0], new Position{Value = new float3(4,0f,0f)});
                entityArray.Dispose();
            }
            #endregion
        }
    }
}
